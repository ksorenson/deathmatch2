package com.kaos.deathmatch.npc;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.Trait;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class NpcManager {

    private Map<String,NPC> npcs = new HashMap<String,NPC>();
    private NPC gunShop;
    private NPC oreTrader;
    private NPC donny;
    private NPC huxham;

    public NpcManager() {
        gunShop = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "TheCrystalFlame");
        oreTrader = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "Tzeentchful");
        donny = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER,"DonnyTheCheff");
        huxham = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER,"Huxham");
        huxham.addTrait(MyTrait.class);
        npcs.put("TheCrystalFlame",gunShop);
        npcs.put("Tzeentchful",oreTrader);
        npcs.put("DonnyTheCheff",donny);
        npcs.put("Huxham",huxham);
    }

    public NPC getNpcByName(String name) {
        return npcs.get(name);
    }






}
