package com.kaos.deathmatch.mining;

import org.bukkit.Material;
import org.bukkit.block.Block;

public class MiningTask implements Runnable {

    private final Block block;
    private final Material old;

    public MiningTask(Block block, Material old) {
        this.old = old;
        this.block = block;
    }

    @Override
    public void run() {
        block.getLocation().getBlock().setType(old);
    }
}
