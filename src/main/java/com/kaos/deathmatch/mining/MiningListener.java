package com.kaos.deathmatch.mining;

import com.kaos.deathmatch.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.scheduler.BukkitTask;

public class MiningListener implements Listener {

    public MiningListener(Main main) {
        this.main = main;
    }

    private final Main main;

    @EventHandler
    private void blockBreakEvent(BlockBreakEvent event) {
        event.setCancelled(true);
        if(event.getBlock().getType()==Material.IRON_ORE) {
            Block block = event.getBlock();
            Material old = event.getBlock().getType();
            block.setType(Material.STONE);
            Bukkit.getScheduler().runTaskLater(main, new MiningTask(event.getBlock(), old), 100l);
        }
    }
}
