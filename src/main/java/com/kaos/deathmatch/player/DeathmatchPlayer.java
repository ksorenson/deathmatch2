package com.kaos.deathmatch.player;

import com.kaos.deathmatch.weapons.*;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class DeathmatchPlayer {

    private Player bukkitPlayer;

    private final Map<Ammo, Integer> ammo = new HashMap<Ammo, Integer>();

    private final Map<Integer,Gun> guns = new HashMap<Integer,Gun>();
    private Gun gun;
    private Gun gun2;
    private Gun gun3;

    private double cryptos = 0d;

    public double getCryptos() {
        return cryptos;
    }

    public void setCryptos(double cryptos) {
        this.cryptos = cryptos;
    }

    public Map<Integer, Gun> getGuns() {
        return guns;
    }

    public DeathmatchPlayer(Player bukkitPlayer) {

        this.bukkitPlayer = bukkitPlayer;
        guns.put(0,createNewGun());
        bukkitPlayer.getInventory().addItem(guns.get(0).createItem());
        this.ammo.put(Ammo.HEAVY,1000);
        this.ammo.put(Ammo.SMALL,1000);
        this.ammo.put(Ammo.MEDIUM,1000);
        this.ammo.put(Ammo.ROCKETS,1000);

    }

    private Gun createNewGun(){
        return new AssaultRifle();
    }

    private Gun createNewGun2(){
        return new Sniper();
    }

    private Gun createNewGun3(){
        return new RPG();
    }

    public int getAmmoByType(Ammo ammo) {
        return this.ammo.get(ammo);
    }

    public void chewAmmo(Ammo type) {

        if(this.getAmmoByType(type)>0) {
            this.ammo.put(type,
                    this.getAmmoByType(type)-1);
        }
    }



    public Player getBukkitPlayer()
    {
        return bukkitPlayer;
    }

    public Gun getGun(int gun)
    {
        return guns.get(gun);
    }

    public void setBukkitPlayer(Player bukkitPlayer)
    {
        this.bukkitPlayer = bukkitPlayer;
    }

    public void setPlayerClass() {
        gun = createNewGun();
    }


}
