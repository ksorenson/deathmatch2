package com.kaos.deathmatch.player;


import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private final Map<UUID, DeathmatchPlayer> players = new HashMap<UUID, DeathmatchPlayer>();

    public Map<UUID, DeathmatchPlayer> getPlayers() {
        return players;
    }

    public void registerPlayer(DeathmatchPlayer player) {
        players.put(player.getBukkitPlayer().getUniqueId(), player);
    }

    public void deRegisterPlayer(UUID uuid) {
        players.remove(uuid);
    }

    public DeathmatchPlayer getPlayer(UUID uuid) {
        return players.get(uuid);
    }

    public DeathmatchPlayer getPlayer(Player player) {
        return getPlayer(player.getUniqueId());
    }

}
