package com.kaos.deathmatch.weapons;

public enum Ammo {

    HEAVY,MEDIUM,SMALL,ROCKETS

}
