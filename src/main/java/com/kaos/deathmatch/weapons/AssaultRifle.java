package com.kaos.deathmatch.weapons;

import com.kaos.deathmatch.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitTask;


public class AssaultRifle extends Gun implements Runnable {

    private BukkitTask task = null;
    private int shotsToFire = 0;
    private Player player;

    private final Material m1 = Material.STONE_HOE;

    public Material getM1() {
        return m1;
    }

    public AssaultRifle() {
        super(480, 50, 8, 20, Material.STONE_HOE, "Assault Rifle", new String[]{
                //MessageConstants.format("Fires three rounds at once."),
                //MessageConstants.format("&6Rate of fire &8[&2=======>&7==&8]"),
                //MessageConstants.format("&4Damage       &8[&2====>&7=====&8]")});
        });
        super.ammoType = Ammo.MEDIUM;
    }

    @Override
    public void onLeftClick(PlayerInteractEvent event) {

    }

    @Override
    public void onRightClick(PlayerInteractEvent event) {
        if(task == null) {
            player = event.getPlayer();
            task = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), this, 0L, 2L);
            shotsToFire = 3;
        }
    }

    @Override
    public void onQPress(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    public void run() {
        if(shotsToFire > 0) {
            new Bullet(player, this.range, this.damage);
            for(Player player : Bukkit.getOnlinePlayers()) {
                player.playSound(player.getLocation(), "dm.rifle", 1f, 10f );
            }
            shotsToFire--;
        } else {
            task.cancel();
            task = null;
        }
    }
}
