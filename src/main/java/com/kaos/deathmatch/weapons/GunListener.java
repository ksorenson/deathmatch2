package com.kaos.deathmatch.weapons;


import com.kaos.deathmatch.main.Main;
import com.kaos.deathmatch.player.DeathmatchPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Iterator;
import java.util.Map;

public class GunListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        DeathmatchPlayer player = Main.getInstance().getPlayerManager().getPlayer(event.getPlayer());
        Gun gun = null;
        Iterator iterator = player.getGuns().entrySet().iterator();
        while(iterator.hasNext()) {
                Map.Entry<Integer, Gun> me = (Map.Entry<Integer,Gun>) iterator.next();
            if(me.getValue()!=null) {
                if (me.getValue().getMaterial().equals(player.getBukkitPlayer().getInventory().getItemInMainHand().getType())) {
                    gun = me.getValue();
                    break;
                }
            }
        }
        if(gun != null) {
            if (gun.getMaterial() == player.getBukkitPlayer().getInventory().getItemInMainHand().getType()) {
                if(player.getAmmoByType(gun.getAmmoType())>1) {
                    switch (event.getAction()) {
                        case LEFT_CLICK_AIR:
                            gun.onLeftClick(event);
                            break;
                        case RIGHT_CLICK_AIR:
                            if (gun.lastShot + gun.rateOfFire <= System.currentTimeMillis()) {
                                gun.onRightClick(event);
                                gun.setLastShot(System.currentTimeMillis());
                                player.chewAmmo(gun.ammoType);
                            }
                            break;
                    }
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onDrop(PlayerDropItemEvent event) {
        DeathmatchPlayer dPlayer = Main.getInstance().getPlayerManager().getPlayer(event.getPlayer());
        Iterator iterator = dPlayer.getGuns().entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry me2 = (Map.Entry) iterator.next();
            if(me2.getValue()instanceof Gun) {
                if (event.getPlayer().getInventory().getItemInMainHand().getType() == ((Gun) me2.getValue()).getMaterial()) {
                    ((Gun) me2.getValue()).onQPress(event);
                }
            }
        }
    }

}
