package com.kaos.deathmatch.weapons;


import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Sniper extends Gun {

    private boolean zoomed = false;
    private boolean farZoom = false;

    private final Material m1 = Material.STONE_AXE;

    public Material getM1() {
        return m1;
    }

    public Sniper() {
        super(1200, 150, 20, 3, Material.STONE_AXE, "Sniper", new String[]{
                //"A bolt acton sniper rifle.",
                //MessageConstants.format("&6Rate of fire &8[&2===>&7======&8]"),
                //MessageConstants.format("&4Damage       &8[&2========>&7=&8]")});
        });
        super.ammoType = Ammo.HEAVY;
    }


    public void onLeftClick(PlayerInteractEvent event) {
        if(zoomed) {
            event.getPlayer().getInventory().setHelmet(null);
            event.getPlayer().removePotionEffect(PotionEffectType.SLOW);
            event.getPlayer().removePotionEffect(PotionEffectType.SPEED);
            event.getPlayer().updateInventory();
            zoomed = false;
            farZoom = false;
        } else {
            event.getPlayer().getInventory().setHelmet(new ItemStack(Material.PUMPKIN));
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1, true));
            event.getPlayer().updateInventory();
            zoomed = true;
        }
    }

    @Override
    public void onRightClick(PlayerInteractEvent event) {
        new Bullet(event.getPlayer(), this.range, this.damage);

        for(Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(event.getPlayer().getLocation(), "dm.sniper", 1f, 35f);
        }
    }

    @Override
    public void onQPress(PlayerDropItemEvent event) {
        if(!zoomed) {
            return;
        }

        if(farZoom) {
            event.getPlayer().removePotionEffect(PotionEffectType.SLOW);
            event.getPlayer().removePotionEffect(PotionEffectType.SPEED);
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1, true));
            farZoom = false;
        } else {
            event.getPlayer().removePotionEffect(PotionEffectType.SLOW);
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 10, true));
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 10, true));
            farZoom = true;
        }
        event.setCancelled(true);
    }

}
