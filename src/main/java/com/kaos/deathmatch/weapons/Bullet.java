package com.kaos.deathmatch.weapons;

import com.kaos.deathmatch.main.Main;
import com.kaos.deathmatch.utils.raytracing.AABB;
import com.kaos.deathmatch.utils.raytracing.Ray3D;
import com.kaos.deathmatch.utils.raytracing.Vec3D;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Bullet {

    private final Player shooter;
    private float range;
    private final int damage;


    public Bullet(Player shooter, float range, int damage) {
        this.range = range;
        this.shooter = shooter;
        this.damage = damage;

        fire(damage);
    }

    public void fire(int damage)
    {
        // Map of players in ray, boolean for headshot.
        final Map<String, Boolean> playersInRay = new HashMap<String, Boolean>();
        final List<LivingEntity> entitiesInRay = new ArrayList<LivingEntity>();

        //3D Vector for the origin of the shot and the direction it is travelling.
        Vec3D origin, direction, tempDir, rotatedDir, pDir, pOrigin;
        origin = Vec3D.fromLocation(shooter.getEyeLocation());
        Location eyeLoc = shooter.getEyeLocation().clone();
        direction = Vec3D.fromVector(eyeLoc.getDirection()).normalize();

        tempDir = Vec3D.fromVector(eyeLoc.getDirection()).normalize();

        rotatedDir = new Vec3D(tempDir.x * Math.cos(45) - tempDir.z * Math.sin(45), tempDir.y - 0.25,
                tempDir.x * Math.sin(45) + tempDir.z * Math.cos(45));

        pDir = new Vec3D(tempDir.x * Math.cos(-5) - tempDir.z * Math.sin(-5), tempDir.y - 0.25,
                tempDir.x * Math.sin(-5) + tempDir.z * Math.cos(-5));

        pOrigin = origin.add(rotatedDir);

        boolean slowfire = shooter.getInventory().getItemInMainHand().getType() == Material.STONE_SWORD;


        // Create the ray
        final Ray3D ray3D = new Ray3D(origin, direction);
        // Create particle ray
        final Ray3D pray3D = new Ray3D(pOrigin, direction);


        // Adjusts range to nearest obstacle if necessary
        BlockIterator iterator = new BlockIterator(shooter, (int) range);
        while (iterator.hasNext()) {
            Block block = iterator.next();
            if (block.getType() != Material.AIR) {
                range = (float) shooter.getEyeLocation().distance(block.getLocation());
                break;
            }
        }


        if(!slowfire) {
            for (LivingEntity entity : shooter.getWorld().getLivingEntities()) {
                if (entity instanceof Player) {
                    continue;
                }

                Vec3D intersectionPoint = getIntersecion(entity, ray3D);
                if (intersectionPoint != null) {
                    entitiesInRay.add(entity);
                }
            }
        }


        // Create a bounding box for all players
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player == shooter) {
                continue;
            }

            Vec3D intersectionPoint = getIntersecion(player, ray3D);
            if (intersectionPoint != null) {
                if (intersectionPoint.y >= player.getLocation().getY() + 1.5) {
                    playersInRay.put(player.getName(), true);
                } else {
                    playersInRay.put(player.getName(), false);
                }
            }
        }

        int particleDistance; // Distance particles will play from the shooters location
        Player nearestHit = null;
        LivingEntity nearestEntHit = null;

        if (slowfire) {
            new BukkitRunnable()
            {
                Player nearestHit = null;
                LivingEntity nearestEntHit = null;
                int particleDistance = (int) Math.floor(range);
                int damage = 50;
                int i = 1;
                public void run()
                {
                    boolean colides = false;

                    if (i <= (int) Math.floor(range)) {
                        Vec3D locr = pray3D.getPointAtDistance(i);
                        Location loc = new Location(shooter.getWorld(), locr.x, locr.y, locr.z);
                        shooter.getWorld().spawnParticle(Particle.CRIT, loc, 1, 0d, 0d, 0d, 0.1);
                        for (LivingEntity entity : shooter.getWorld().getLivingEntities()) {
                            if (entity instanceof Player) {
                                continue;
                            }

                            Vec3D intersectionPoint = getIntersecion(entity, ray3D);
                            if (intersectionPoint != null) {
                                entitiesInRay.add(entity);
                            }
                        }
                        if (!playersInRay.isEmpty()) { // A player is hit
                            nearestHit = getNearestHit(playersInRay, shooter);
                            particleDistance = (int) Math.floor(shooter.getLocation().distance(nearestHit.getLocation()));

                            if (playersInRay.get(nearestHit.getName())) { //headshot code
                                damage *= 2;
                            }
                            nearestHit.damage(damage, shooter);

                        } else if (!entitiesInRay.isEmpty()) { // An entity is hit
                            nearestEntHit = getNearestEntHit(entitiesInRay, shooter);
                            particleDistance = (int) Math.floor(shooter.getLocation().distance(nearestEntHit.getLocation()));

                        } else { // No players hit
                            shooter.getWorld().spawnParticle(Particle.FLAME, new Location(shooter.getWorld(),loc.getX()-0.025,loc.getY()+0.025, loc.getZ()-0.025), 2, 0d, 0d, 0d, 0);
                            shooter.getWorld().spawnParticle(Particle.FLAME, new Location(shooter.getWorld(),loc.getX()-0.025,loc.getY()-0.025, loc.getZ()-0.025), 2, 0d, 0d, 0d, 0);
                            shooter.getWorld().spawnParticle(Particle.FLAME, new Location(shooter.getWorld(),loc.getX()+0.025,loc.getY()+0.025, loc.getZ()+0.025), 2, 0d, 0d, 0d, 0);
                            shooter.getWorld().spawnParticle(Particle.FLAME, new Location(shooter.getWorld(),loc.getX()+0.025,loc.getY()-0.025, loc.getZ()+0.025), 2, 0d, 0d, 0d, 0);
                            shooter.getWorld().spawnParticle(Particle.SMOKE_NORMAL, loc, 3, 0d, 0d, 0d, 0.05);
                        }

                        if (colides || i == (int) Math.floor(range)) {
                            if(nearestEntHit != null)nearestEntHit.damage(damage, shooter);
                            shooter.getWorld().createExplosion(loc,3);
                            cancel();

                        }
                        i++;
                    }
                }
            }.runTaskTimer(Main.getInstance(),0,1);
        } else {

            if (!playersInRay.isEmpty()) { // A player is hit
                nearestHit = getNearestHit(playersInRay, shooter);
                particleDistance = (int) Math.floor(shooter.getLocation().distance(nearestHit.getLocation()));

                if (playersInRay.get(nearestHit.getName())) { //headshot code
                    damage *= 2;
                }
                nearestHit.damage(damage, shooter);

            } else { // No players hit
                particleDistance = (int) Math.floor(range);
            }

            if (!entitiesInRay.isEmpty()) { // An entity is hit
                nearestEntHit = getNearestEntHit(entitiesInRay, shooter);
                particleDistance = (int) Math.floor(shooter.getLocation().distance(nearestEntHit.getLocation()));
                nearestEntHit.damage(damage, shooter);

            } else { // No players hit
                particleDistance = (int) Math.floor(range);
            }

        /*float angle = shooter.getEyeLocation().getYaw() / 60;
        Location handLoc = shooter.getEyeLocation().add(Math.cos(angle), 0, Math.sin(angle));
        if (!playersInRay.isEmpty()) {
        Vec3D intersect = getIntersecion(nearestHit, ray3D);
        }*/

            //  Ray3D particleRay = new Ray3D(Vec3D.fromLocation(handLoc), intersect.);


            // Particle trail

            for (int i = 1; i <= particleDistance; i += 4) {

                if (i > particleDistance) {
                    i = particleDistance;
                }

                Vec3D locr = pray3D.getPointAtDistance(i);
                Location loc = new Location(shooter.getWorld(), locr.x, locr.y, locr.z);
                shooter.getWorld().spawnParticle(Particle.CRIT, loc, 1, 0d, 0d, 0d, 0.1);
                //ParticleEffect.CRIT.display(loc, 16, 0, 0, 0, 0F, 1);
            }
        }
    }




    /**
     * Gets the player closest to the specified shooter.
     * We use the {@link org.bukkit.Location#distanceSquared(org.bukkit.Location) distanceSquared}
     * to reduce the cycles taken by removing the square root calculation.
     *
     * @param players players to check distance on.
     * @param player the shooter to use as the origin.
     *
     * @return The player closest to the origin
     */
    public Player getNearestHit(Map<String, Boolean> players, Player player) {
        String closest = null;
        Double distance = null;
        for (Map.Entry<String, Boolean> current : players.entrySet()) {
            Player hitPlayer = Bukkit.getPlayer(current.getKey());
            if (distance != null) {
                if (player.getLocation().distanceSquared(hitPlayer.getLocation()) < distance) {
                    distance = player.getLocation().distanceSquared(hitPlayer.getLocation());
                    closest = current.getKey();
                }
            } else {
                distance = player.getLocation().distanceSquared(hitPlayer.getLocation());
                closest = current.getKey();
            }
        }
        return Bukkit.getPlayer(closest);
    }

    public LivingEntity getNearestEntHit(List<LivingEntity> entities, Player player) {
        LivingEntity closest = null;
        Double distance = null;
        for (LivingEntity current: entities) {
            if (distance != null) {
                if (player.getLocation().distanceSquared(current.getLocation()) < distance) {
                    distance = player.getLocation().distanceSquared(current.getLocation());
                    closest = current;
                }
            } else {
                distance = player.getLocation().distanceSquared(current.getLocation());
                closest = current;
            }
        }
        return closest;
    }

    private Vec3D getIntersecion(Player player, Ray3D ray) {
        Location min = player.getLocation().subtract(0.5,0,0.5);
        Location max = player.getLocation().add(0.5, 2, 0.5);
        AABB boundingBox = new AABB(
                new Vec3D(min.getX(), min.getY(), min.getZ()),
                new Vec3D(max.getX(), max.getY(), max.getZ()));

        return boundingBox.intersectsRay(ray, 1.5F, range);
    }

    private Vec3D getIntersecion(Entity entity, Ray3D ray) {
        Location min = entity.getLocation().subtract(0.75,0,0.75);
        Location max = entity.getLocation().add(0.75, 1.5, 0.75);
        AABB boundingBox = new AABB(
                new Vec3D(min.getX(), min.getY(), min.getZ()),
                new Vec3D(max.getX(), max.getY(), max.getZ()));

        return boundingBox.intersectsRay(ray, 1.5F, range);
    }

}
