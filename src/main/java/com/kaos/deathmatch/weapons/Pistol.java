package com.kaos.deathmatch.weapons;

import com.kaos.deathmatch.utils.MessageConstants;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class Pistol extends Gun {

    private final Material m1 = Material.WOOD_HOE;

    public Material getM1() {
        return m1;
    }

    public Pistol() {
        super(480, 20, 10, 6, Material.WOOD_HOE, "Pistol", new String[]{
                MessageConstants.format("&6Rate of fire &8[&2===>&7=====&4]"),
                MessageConstants.format("&4Damage       &8[&2======>&7===&8]")});
        super.ammoType = Ammo.MEDIUM;
    }

    @Override
    public void onLeftClick(PlayerInteractEvent event) {

    }

    @Override
    public void onRightClick(PlayerInteractEvent event) {
        new Bullet(event.getPlayer(), this.range, this.damage);
    }

    @Override
    public void onQPress(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }
}
