package com.kaos.deathmatch.weapons;


import com.kaos.deathmatch.utils.MessageConstants;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class P99 extends Gun {

    private final Material m1 = Material.GOLD_SPADE;

    public Material getM1() {
        return m1;
    }

    public P99() {
        super(100, 50, 6, 30, Material.GOLD_SPADE, "P99 Auto", new String[]{
                MessageConstants.format("&6Rate of fire &8[&2=======>&7==&8]"),
                MessageConstants.format("&4Damage       &8[&2====>&7=====&8]")});
        super.ammoType = Ammo.MEDIUM;
    }

    public void onLeftClick(PlayerInteractEvent event) {

    }

    @Override
    public void onRightClick(PlayerInteractEvent event) {
        new Bullet(event.getPlayer(), this.range, this.damage);

        for(Player player : Bukkit.getOnlinePlayers()) {
            //player.playSound(event.getPlayer().getLocation(), "dm.sniper", 1f, 35f);
        }
    }

    public void onQPress(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

}
