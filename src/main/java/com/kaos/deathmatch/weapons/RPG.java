package com.kaos.deathmatch.weapons;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class RPG extends Gun implements Runnable {

    private final Material m1 = Material.STONE_SWORD;

    public Material getM1() {
        return m1;
    }

    public RPG()
    {
        super(3000, 75, 100, 1, Material.STONE_SWORD, "RPG", new String[]{
                //MessageConstants.format("Fires three rounds at once."),
                //MessageConstants.format("&6Rate of fire &8[&2=======>&7==&8]"),
                //MessageConstants.format("&4Damage       &8[&2====>&7=====&8]")});
        });
        super.ammoType = Ammo.ROCKETS;
    }

    public void onLeftClick(PlayerInteractEvent event)
    {

    }

    public void onRightClick(PlayerInteractEvent event)
    {
        new Bullet(event.getPlayer(), this.range, this.damage);
    }

    public void onQPress(PlayerDropItemEvent event)
    {
        event.setCancelled(true);
    }

    public void run()
    {

    }
}

