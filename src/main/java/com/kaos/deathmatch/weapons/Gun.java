package com.kaos.deathmatch.weapons;

import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class Gun {


    protected Ammo ammoType;
    protected int currentClip;
    protected long rateOfFire;
    protected int damage;
    protected int clipSize;
    protected int range;
    protected ItemStack item;

    public ItemStack getItem() {
        return item;
    }

    public long getLastShot()
    {
        return lastShot;
    }

    public void setLastShot(long lastShot)
    {
        this.lastShot = lastShot;
    }

    protected long lastShot = 0L;

    public Material getMaterial()
    {
        return material;
    }

    private final Material material;
    private final String name;
    private final String[] lore;


    protected Gun(long rateOfFire, int range, int damage, int clipSize, Material material, String name, String[] lore) {
        this.rateOfFire = rateOfFire;
        this.range = range;
        this.damage = damage;
        this.clipSize = clipSize;
        this.material = material;
        this.name = name;
        this.lore = lore;
    }

    public ItemStack createItem() {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Lists.newArrayList(lore));
        item.setItemMeta(meta);
        this.item = item;
        return item;
    }

    public abstract void onLeftClick(PlayerInteractEvent event);

    public abstract void onRightClick(PlayerInteractEvent event);

    public abstract void onQPress(PlayerDropItemEvent event);

    public Ammo getAmmoType() {
        return ammoType;
    }

    public int getCurrentClip() {
        return currentClip;
    }

    public void setAmmoType(Ammo ammoType) {
        this.ammoType = ammoType;
    }

    public void setCurrentClip(int currentClip) {
        this.currentClip = currentClip;
    }
}
