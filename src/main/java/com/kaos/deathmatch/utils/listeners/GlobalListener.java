package com.kaos.deathmatch.utils.listeners;

import com.kaos.deathmatch.main.Main;
import com.kaos.deathmatch.player.DeathmatchPlayer;
import com.kaos.deathmatch.player.PlayerManager;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;

public class GlobalListener implements Listener {

    private PlayerManager pm;

    @EventHandler(priority = EventPriority.LOW)
    public void onJoin(PlayerJoinEvent event) {
        pm = Main.getInstance().getPlayerManager();
        pm.registerPlayer(new DeathmatchPlayer(event.getPlayer()));
        DeathmatchPlayer pmPlayer = pm.getPlayer(event.getPlayer());
        event.getPlayer().getInventory().clear();
        event.getPlayer().getInventory().addItem(pmPlayer.getGun(0).createItem());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event) {
        Main.getInstance().getPlayerManager().deRegisterPlayer(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void noHunger(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent event) {
        //event.setCancelled(true);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        //event.setCancelled(true);
    }

    @EventHandler
    public void onMobSpawn(CreatureSpawnEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public  void onWeatherChange(WeatherChangeEvent event) {
        //event.setCancelled(true);
    }

    @EventHandler
    private void onInteract(PlayerInteractEvent event) {
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if(event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.STICK)) {
                NPC npc = Main.getInstance().getNpcManager().getNpcByName("Tzeentchful");
                if (npc.isSpawned()) {
                    npc.teleport(event.getPlayer().getLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                } else{
                    npc.spawn(event.getPlayer().getLocation());
                }
            }
            if(event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.SUGAR)) {
                NPC npc = Main.getInstance().getNpcManager().getNpcByName("Huxham");
                if (npc.isSpawned()) {
                    npc.teleport(event.getPlayer().getLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                } else{
                    npc.spawn(event.getPlayer().getLocation());
                }
            }
        }
        if(event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if (event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.STICK)) {
                NPC npc2 = Main.getInstance().getNpcManager().getNpcByName("TheCrystalFlame");
                if (npc2.isSpawned()) {
                    npc2.teleport(event.getPlayer().getLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                } else {
                    npc2.spawn(event.getPlayer().getLocation());
                }
            }
        }
    }

    @EventHandler
    public void npcRightClickEvent(NPCRightClickEvent event) {
        if(event.getNPC().getFullName().equals("TheCrystalFlame")){
            Main.getInstance().getOreTrader().openInventory(event.getClicker());
        }else if(event.getNPC().getFullName().equals("Tzeentchful")) {
            Main.getInstance().getGunShop().openInventory(event.getClicker());
        }
    }



}
