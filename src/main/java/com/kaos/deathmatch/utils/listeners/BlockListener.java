package com.kaos.deathmatch.utils.listeners;

import com.kaos.deathmatch.main.Main;
import com.kaos.deathmatch.utils.runnables.OreRunnable;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

public class BlockListener implements Listener {

    @EventHandler
    private void blockBreakEvent(BlockBreakEvent event) {
        Block block = event.getBlock();
        BukkitTask oreRunnable;
        if(!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.SUGAR)) {
            switch (block.getType()) {
                case IRON_ORE:
                    oreRunnable = new OreRunnable(Main.getInstance(), block).runTaskLater(Main.getInstance(), 900L);
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.IRON_ORE));
                    break;
                case GOLD_ORE:
                    oreRunnable = new OreRunnable(Main.getInstance(), block).runTaskLater(Main.getInstance(), 2000L);
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.GOLD_ORE));
                    break;
                case DIAMOND_ORE:
                    oreRunnable = new OreRunnable(Main.getInstance(), block).runTaskLater(Main.getInstance(), 4000L);
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.DIAMOND));
                    break;
                case COAL_ORE:
                    oreRunnable = new OreRunnable(Main.getInstance(), block).runTaskLater(Main.getInstance(), 400L);
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.COAL_ORE));
                    break;
                default:
                    break;
            }

            event.setCancelled(true);
        }
    }
}
