package com.kaos.deathmatch.utils.runnables;

import com.kaos.deathmatch.main.Main;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class OreRunnable extends BukkitRunnable {

    private final Main main;

    private final Material material;
    private final Block block;

    public OreRunnable(Main main, Block block) {
        this.main = main;
        this.material = block.getType();
        block.setType(Material.STONE);
        this.block = block;
    }

    @Override
    public void run() {
        block.setType(material);
    }
}
