package com.kaos.deathmatch.utils.raytracing;

import org.bukkit.util.Vector;

public class Vec2D {

    public double getX()
    {
        return x;
    }

    public double getZ()
    {
        return z;
    }

    private final double x;
    private final double z;

    public Vec2D(double x, double y) {
        this.x = x;
        this.z = y;
    }

    public Vec2D(Vector bukkitVector) {
        this.x = bukkitVector.getX();
        this.z = bukkitVector.getZ();
    }

}
