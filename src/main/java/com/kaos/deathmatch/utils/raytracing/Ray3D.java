package com.kaos.deathmatch.utils.raytracing;


import org.bukkit.Location;

public class Ray3D extends Vec3D {

    public Vec3D getDir()
    {
        return dir;
    }

    public final Vec3D dir;

    public Ray3D(Vec3D origin, Vec3D direction) {
        super(origin);
        this.dir = direction.normalize();
    }

    /**
     * Construct a 3D ray from a location.
     * @param location - the Bukkit location.
     */
    public Ray3D(Location location) {
        this(fromLocation(location), fromVector(location.getDirection()));
    }

    public Vec3D getPointAtDistance(double dist) {
        return add(dir.scale(dist));
    }

    @Override
    public String toString() {
        return "origin: " + super.toString() + " direction: " + dir;
    }

}
