package com.kaos.deathmatch.utils.shops;

import com.kaos.deathmatch.main.Main;
import com.kaos.deathmatch.player.DeathmatchPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class OreTrader implements Listener {

    private final Inventory inv;

    public OreTrader() {
        // Create a new inventory, with no owner (as this isn't a real inventory), a size of nine, called example
        inv = Bukkit.createInventory(null, 9, "Ore Trader");
        Main.getInstance().getServer().getPluginManager().registerEvents(this,Main.getInstance());
        // Put the items into the inventory
        initializeItems();
    }

    // You can call this whenever you want to put the items in
    public void initializeItems() {
        inv.addItem(createGuiItem(Material.IRON_ORE, "Iron Ore", "§avaluable metal resource", "§bworth $5 cryptos"));
        inv.addItem(createGuiItem(Material.GOLD_ORE, "§bGold Ore", "§aBitcoin Golds Predecessor", "§blike bitcoingold, but shiny"));
        inv.addItem(createGuiItem(Material.DIAMOND, "§bDiamond", "§a50Diamond", "§bChicks love these"));
        inv.addItem(createGuiItem(Material.COAL, "§cCoal", "§asomeones been naughty this year", "§bgift from kraumpus."));
    }

    // Nice little method to create a gui item with a custom name, and description
    protected ItemStack createGuiItem(final Material material, final String name, final String... lore) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        // Set the name of the item
        meta.setDisplayName(name);

        // Set the lore of the item
        meta.setLore(Arrays.asList(lore));

        item.setItemMeta(meta);

        return item;
    }

    // You can open the inventory with this
    public void openInventory(final HumanEntity ent) {
        ent.openInventory(inv);
    }

    // Check for clicks on items
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        if(e.getWhoClicked()instanceof Player) {
            DeathmatchPlayer dPlayer = Main.getInstance().getPlayerManager().getPlayer((Player) e.getWhoClicked());
            if (e.getInventory() != inv) return;

            final ItemStack clickedItem = e.getCurrentItem();
            final Player p = (Player) e.getWhoClicked();

            if(e.getClick()== ClickType.RIGHT){
                if(clickedItem.getType()==Material.IRON_ORE){
                    sellItem(e.getCurrentItem(), dPlayer);
                    e.getWhoClicked().sendMessage("your new balance is " + dPlayer.getCryptos());
                }
            } else if(e.getClick()==ClickType.LEFT){
                if(clickedItem.getType()==Material.IRON_ORE){
                    if (dPlayer.getCryptos()>=Main.getInstance().getShopManager().getPrice(Material.IRON_ORE)){
                        buyItem(e.getCurrentItem(),dPlayer);
                        e.getWhoClicked().sendMessage("your new balance is " + dPlayer.getCryptos());
                    } else {
                        e.getWhoClicked().sendMessage("you cannot afford this item.");
                    }
                }else if (clickedItem.getType()==Material.DIAMOND) {
                    if (dPlayer.getCryptos()>=Main.getInstance().getShopManager().getPrice(Material.DIAMOND)){
                        buyItem(e.getCurrentItem(),dPlayer);
                        e.getWhoClicked().sendMessage("your new balance is " + dPlayer.getCryptos());
                    } else {
                        e.getWhoClicked().sendMessage("you cannot afford this item.");
                    }
                }else if (clickedItem.getType()==Material.GOLD_ORE) {
                    if (dPlayer.getCryptos()>=Main.getInstance().getShopManager().getPrice(Material.GOLD_ORE)){
                        buyItem(e.getCurrentItem(),dPlayer);
                        e.getWhoClicked().sendMessage("your new balance is " + dPlayer.getCryptos());
                    } else {
                        e.getWhoClicked().sendMessage("you cannot afford this item.");
                    }
                }else if(clickedItem.getType()==Material.COAL) {
                    if (dPlayer.getCryptos()>=Main.getInstance().getShopManager().getPrice(Material.DIAMOND)){
                        buyItem(e.getCurrentItem(),dPlayer);
                        e.getWhoClicked().sendMessage("your new balance is " + dPlayer.getCryptos());
                    } else {
                        e.getWhoClicked().sendMessage("you cannot afford this item.");
                    }
                }
            }
            e.setCancelled(true);





            // verify current item is not null
            if (clickedItem == null || clickedItem.getType() == Material.AIR) return;



            // Using slots click is a best option for your inventory click's
            //p.sendMessage("You clicked at slot " + e.getRawSlot());
        }
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory() == inv) {
            e.setCancelled(true);
        }
    }

    private void sellItem(ItemStack item, DeathmatchPlayer player) {
        if(item!=null) {
            if(item.getType()!=Material.AIR) {
                ShopManager shopManager = Main.getInstance().getShopManager();
                double cryptos = player.getCryptos();
                double price = shopManager.getPrice(item.getType());
                player.setCryptos(cryptos+price);
            }
        }
    }

    private void buyItem(ItemStack item, DeathmatchPlayer player) {
        if(item!=null) {
            if (item.getType() != Material.AIR) {
                ShopManager shopManager = Main.getInstance().getShopManager();
                double cryptos = player.getCryptos();
                double price = shopManager.getPrice(item.getType());
                if (cryptos >= price) player.setCryptos(cryptos - price);
            }
        }
    }

}
