package com.kaos.deathmatch.utils.shops;

import org.bukkit.Material;

import java.util.HashMap;
import java.util.Map;

public class ShopManager {

    private final Map<Material,Double> priceTable = new HashMap<Material, Double>();

    public double getPrice(Material m){
        if(priceTable.get(m)!=null)return priceTable.get(m);
        else return 0d;
    }

    private void setPrice(Material m, double price) {
        priceTable.put(m,price);
    }

    private Material testMat = null;

    public ShopManager() {
        testMat = Material.IRON_ORE;
        priceTable.put(testMat,5d);
        testMat = Material.DIAMOND;
        priceTable.put(testMat, 50d);
        testMat = Material.COAL;
        priceTable.put(testMat, 2.5d);
        testMat = Material.GOLD_ORE;
        priceTable.put(testMat,25d);
        testMat = Material.STONE_HOE;
        priceTable.put(testMat, 100d);
    }






}
