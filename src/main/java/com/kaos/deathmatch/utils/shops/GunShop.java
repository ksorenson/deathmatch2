package com.kaos.deathmatch.utils.shops;

import com.kaos.deathmatch.main.Main;
import com.kaos.deathmatch.player.DeathmatchPlayer;
import com.kaos.deathmatch.weapons.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class GunShop implements Listener {
    private final Inventory inv;

    public GunShop() {
        // Create a new inventory, with no owner (as this isn't a real inventory), a size of nine, called example
        inv = Bukkit.createInventory(null, 9, "Gun Shop");
        Main.getInstance().getServer().getPluginManager().registerEvents(this,Main.getInstance());
        // Put the items into the inventory
        initializeItems();
    }

    // You can call this whenever you want to put the items in
    public void initializeItems() {
        inv.addItem(createGuiItem(Material.STONE_HOE, "Assault Rifle", "§astandard issue", "§bfires 3 shot burst rounds."));
        inv.addItem(createGuiItem(Material.STONE_SWORD, "§bRPG", "§aMissile launcher", "§bgood for blowing things up."));
        inv.addItem(createGuiItem(Material.STONE_AXE, "§bSniper Rifle", "§a50 caliber", "§bdeath from afar."));
        inv.addItem(createGuiItem(Material.GOLD_SPADE, "§cP99", "§asub machine gun", "§bfully automatic."));
        inv.addItem(createGuiItem(Material.WOOD_HOE, "§cPistol", "§aPistol", "§bgood damge low range"));
    }

    // Nice little method to create a gui item with a custom name, and description
    protected ItemStack createGuiItem(final Material material, final String name, final String... lore) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        // Set the name of the item
        meta.setDisplayName(name);

        // Set the lore of the item
        meta.setLore(Arrays.asList(lore));

        item.setItemMeta(meta);

        return item;
    }

    // You can open the inventory with this
    public void openInventory(final HumanEntity ent) {
        ent.openInventory(inv);
    }

    // Check for clicks on items
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        if (e.getInventory() != inv) return;

        e.setCancelled(true);

        final ItemStack clickedItem = e.getCurrentItem();

        if(e.getWhoClicked()instanceof Player) {
            DeathmatchPlayer dPlayer = Main.getInstance().getPlayerManager().getPlayer((Player) e.getWhoClicked());
            int size = dPlayer.getGuns().size();
            if(clickedItem.getType()==Material.STONE_HOE){
                dPlayer.getGuns().put(size, new AssaultRifle());
                e.getWhoClicked().getInventory().addItem(dPlayer.getGuns().get(size).createItem());
            }else if (clickedItem.getType()==Material.STONE_AXE) {
                dPlayer.getGuns().put(size, new Sniper());
                e.getWhoClicked().getInventory().addItem(dPlayer.getGuns().get(size).createItem());
            }else if (clickedItem.getType()==Material.STONE_SWORD) {
                dPlayer.getGuns().put(size,new RPG());
                e.getWhoClicked().getInventory().addItem(dPlayer.getGuns().get(size).createItem());
            }else if(clickedItem.getType()==Material.GOLD_SPADE) {
                dPlayer.getGuns().put(size,new P99());
                e.getWhoClicked().getInventory().addItem(dPlayer.getGuns().get(size).createItem());
            }else if(clickedItem.getType()==Material.WOOD_HOE) {
                dPlayer.getGuns().put(size,new Pistol());
                e.getWhoClicked().getInventory().addItem(dPlayer.getGuns().get(size).createItem());
            }
        }

        // verify current item is not null
        if (clickedItem == null || clickedItem.getType() == Material.AIR) return;

        final Player p = (Player) e.getWhoClicked();

        // Using slots click is a best option for your inventory click's
        p.sendMessage("You clicked at slot " + e.getRawSlot());
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory() == inv) {
            e.setCancelled(true);
        }
    }

    private void sellItem(ItemStack item, DeathmatchPlayer player) {
        ShopManager shopManager = Main.getInstance().getShopManager();
        double cryptos = player.getCryptos();
        double price = shopManager.getPrice(item.getType());
        player.setCryptos(cryptos+price);
    }

    private void buyItem(ItemStack item, DeathmatchPlayer player) {
        ShopManager shopManager = Main.getInstance().getShopManager();
        double cryptos = player.getCryptos();
        double price = shopManager.getPrice(item.getType());
        if(cryptos>=price) player.setCryptos(cryptos-price);
    }
}
