package com.kaos.deathmatch.utils.books;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class BookManager {

    private ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    private BookMeta bookMeta = (BookMeta) book.getItemMeta();

    public void setupTestBook() {
        bookMeta.setAuthor("TheCrystalFlame");
        bookMeta.setTitle("Test Book");
        bookMeta.setDisplayName("book");
        bookMeta.addPage("hello");
    }

}
