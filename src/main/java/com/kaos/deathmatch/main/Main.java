package com.kaos.deathmatch.main;

import com.kaos.deathmatch.npc.MyTrait;
import com.kaos.deathmatch.npc.NpcManager;
import com.kaos.deathmatch.player.PlayerManager;
import com.kaos.deathmatch.utils.listeners.BlockListener;
import com.kaos.deathmatch.utils.listeners.GlobalListener;
import com.kaos.deathmatch.utils.shops.GunShop;
import com.kaos.deathmatch.utils.shops.OreTrader;
import com.kaos.deathmatch.utils.shops.ShopManager;
import com.kaos.deathmatch.weapons.GunListener;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.DespawnReason;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private GunShop gunShop;
    private OreTrader oreTrader;
    private ShopManager shopManager;
    private NpcManager npcManager;

    public NpcManager getNpcManager() {
        return npcManager;
    }

    public ShopManager getShopManager() {
        return shopManager;
    }

    public OreTrader getOreTrader() {
        return oreTrader;
    }


    public GunShop getGunShop() {
        return gunShop;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    private PlayerManager playerManager;

    private static Main instance = null;

    public static Main getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        CitizensAPI.getNPCRegistry().despawnNPCs(DespawnReason.PLUGIN);
        playerManager = new PlayerManager();
        instance = this;
        this.getServer().getPluginManager().registerEvents(new GlobalListener(), this);
        this.getServer().getPluginManager().registerEvents(new GunListener(), this);
        this.getServer().getPluginManager().registerEvents(new BlockListener(), this);
        gunShop = new GunShop();
        oreTrader = new OreTrader();
        shopManager = new ShopManager();
        net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(MyTrait.class).withName("MyTrait"));
        npcManager = new NpcManager();
    }

    @Override
    public void onDisable()
    {
        CitizensAPI.getNPCRegistry().despawnNPCs(DespawnReason.PLUGIN);
        CitizensAPI.getNPCRegistry().deregisterAll();
        CitizensAPI.shutdown();
    }
}
